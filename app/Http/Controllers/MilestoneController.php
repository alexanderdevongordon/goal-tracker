<?php

namespace App\Http\Controllers;

use Auth;
use App\Goal;
use App\Milestone;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Responses\Response;

class MilestoneController extends Controller
{
	public function read(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(), [
			'id' => 'required|exists:milestones,id'
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		return Response::response(array_first(Milestone::for($user->id, ['id' => $request->get('id')])));
	}

	public function all(Request $request)
	{
		$user = User::validate($request);

		return Response::response(Milestone::for($user->id));
	}

	/**
	 * Idea: in another route allow a requestor to create a goal and multiple milestones alltogether.
	 */
	public function create(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(), [
			'goal_id' => 'required|exists:goals,id',
			'name' => 'required|unique:milestones,name',
			'description' => 'required',
			'target_date' => 'required',
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		//Retrieve the goal to ensure this user has access, otherwise 404.
		$goal = Goal::where('user_id', $user->id)
			->where('id', $request->get('goal_id'))
			->firstOrFail();

		$mile = new Milestone;
		$mile->goal_id = $goal->id;
		$mile->name = $request->get('name');
		$mile->description = $request->get('description');
		$mile->target_date = $request->get('target_date');
		$mile->is_complete = false;
		return Response::bool($mile->save());

		//If the goal was complete already, maybe set it to incomplete now that there's a new milestone?
	}

	public function update(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(),[
			'id' => 'required|exists:milestones,id',
			'name' => 'unique:milestones,name',
			'is_complete' => 'boolean',
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		//User id is stored with the goal, not the milestone, so just make sure the milestone is for this user.
		if(empty(Milestone::for($user->id, ['id' => $request->get('id')])))
		{
			abort(403);
		}

		$milestone = Milestone::find($request->get('id'));

		//We'll only change the values that have been given.
		if($request->has('name'))
		{
			$milestone->name = $request->get('name');
		}
		if($request->has('description'))
		{
			$milestone->description = $request->get('description');
		}
		if($request->has('target_date'))
		{
			$milestone->target_date = $request->get('target_date');
		}
		if($request->has('is_complete'))
		{
			//If this was the last milestone marked as complete, then complete the goal.
			if(
				!$milestone->is_complete && $request->get('is_complete')
				&& $milestone->goal->numMilestones - $milestone->goal->numComplete == 1
			)
			{
				$milestone->goal->is_complete = true;
				$milestone->goal->save();
			}
			$milestone->is_complete = $request->get('is_complete');
		}

		return Response::bool($milestone->save());
	}

	public function delete(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(),
			['id' => 'required|exists:milestones,id']
		);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		if(empty(Milestone::for($user->id, ['id' => $request->get('id')])))
		{
			abort(403);
		}

		$milestone = Milestone::find($request->get('id'));

		return Response::bool($milestone->delete());
	}
}
