<?php

namespace App\Http\Controllers;

use Auth;
use App\Goal;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Responses\Response;

class GoalController extends Controller
{
	public function read(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(),
			['id' => 'required|exists:goals,id']
		);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		$goal = Goal::where('user_id', $user->id)
			->where('id', $request->get('id'))
			->with('milestones')
			->firstOrFail();

		//Sets this attribute to be calculated and returned.
		$goal->progress = $goal->progress;

		return Response::response($goal);
	}

	public function all(Request $request)
	{
		$user = User::validate($request);

		return Response::response(Goal::where('user_id', $user->id)->get());
	}

	/**
	 * Given an object of goal attributes, create a new goal and save.
	 * @return $saved - whether or not the goal saved
	 */
	public function create(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:goals,name',
			'description' => 'required',
			'target_date' => 'required',
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		$goal = new Goal;
		$goal->user_id = $user->id;
		$goal->name = $request->get('name');
		$goal->description = $request->get('description');
		$goal->target_date = $request->get('target_date');
		$goal->is_complete = false;
		return Response::bool($goal->save());
	}

	public function update(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(),[
			'id' => 'required|exists:goals,id',
			'name' => 'unique:goals,name',
			'is_complete' => 'boolean',
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		$goal = Goal::where('user_id', $user->id)
			->where('id', $request->get('id'))
			->firstOrFail();

		//We'll only change the values that have been given.
		if($request->has('name'))
		{
			$goal->name = $request->get('name');
		}
		if($request->has('description'))
		{
			$goal->description = $request->get('description');
		}
		//Should this be allowed to be changed? If you can change when your want to fulfill your goals, are they effective?
		if($request->has('target_date'))
		{
			$goal->target_date = $request->get('target_date');
		}
		if($request->has('is_complete'))
		{
			$goal->is_complete = $request->get('is_complete');
			/* Turn this off for now as the user may actually want to know which milestones were not completed.
			//Set all milestones for this goal (most likely to complete).
			foreach($goal->milestones as $milestone)
			{
				$milestone->is_complete = $request->get('is_complete');
				$milestone->save();
			}
			*/
		}

		return Response::bool($goal->save());
	}

	public function delete(Request $request)
	{
		$user = User::validate($request);

		$validator = Validator::make($request->all(), [
			'id' => 'required|exists:goals,id'
		]);
		if($validator->fails())
		{
			return Response::error($validator->errors());
		}

		$goal = Goal::where('user_id', $user->id)
			->where('id', $request->get('id'))
			->firstOrFail();

		return Response::bool($goal->delete());
	}
}
