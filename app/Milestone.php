<?php

namespace App;

use DB;
use App\Goal;
use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
	/**
	 * Return either
	 */
    public static function for($user_id, $extra=[])
    {
		$query = "SELECT
				m.id,
				m.goal_id,
				m.name,
				m.description,
				m.target_date,
				m.is_complete,
				m.created_at,
				m.updated_at
			From milestones m
			inner join goals g
			on m.goal_id = g.id
			where g.user_id = ?";
		$params = [$user_id];

		if(isset($extra['goal_id']))
		{
			$query .= " and m.goal_id = ?";
			$params[] = $extra['goal_id'];
		}
		if(isset($extra['id']))
		{
			$query .= " and m.id = ?";
			$params[] = $extra['id'];
		}

		return DB::select($query, $params);
    }

    public function goal()
    {
    	return $this->belongsTo(Goal::class);
    }
}
