<?php

namespace App;

use App\Milestone;
use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
	public function milestones()
	{
		return $this->hasMany(Milestone::class);
	}

	/**
	 * This is a custom accessor for a field that is not actually in the table this model is for.
	 * We recalculate this as the number of total milestones may have changed,
	 * 	so do some simple math as opposed to updating the goal each time a milestone is changed.
	 */
	public function getProgressAttribute()
	{
		$numMilestones = $this->numMilestones;
		//Don't divide by 0.
		$numMilestones = $numMilestones ?: 1;
		return ($this->numComplete/$numMilestones)*100;
	}

	public function getNumCompleteAttribute()
	{
		return $this->milestones->where('is_complete', 1)->count();
	}

	public function getNumMilestonesAttribute()
	{
		return $this->milestones->count();
	}
}
