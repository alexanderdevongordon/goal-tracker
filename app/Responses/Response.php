<?php

namespace App\Responses;

class Response
{
	/**
	 * Denotes whether or not the request succeeded.
	 */
	public $success = true;

	/**
	 * A list of errors that happen during any process.
	 */
	public $errors = [];

	/**
	 * Holds whatever extra response is required.
	 */
	public $response = null;

	/**
	 * Set and send errors and success is false automatically.
	 * @param $errors - an array of errors that comes back, usually in the form ['name' => [], 'id' => []]
	 * @param $status - HTTP Status Code of the error type. 400 by default to signal a bad request
	 */
	public static function error($errors, $status=400)
	{
		$instance = new self;
		$instance->errors = $errors;
		$instance->success = false;
		return response()->json($instance, $status);
	}

	public static function bool($success)
	{
		$instance = new self;
		$instance->success = $success;
		return response()->json($instance);
	}

	public static function response($response)
	{
		$instance = new self;
		$instance->response = $response;
		return response()->json($instance);
	}
}