
## Setup

1. Install [composer](https://getcomposer.org/)
2. `composer install`
3. `touch database/database.sqlite` (just creates the file)
4. `php artisan migrate`
5. `php artisan serve --port=8080`
6. Browse to http://127.0.0.1:8080 and look around
7. Make API requests


## Testing

1. While the website is running, visit and register for an account
   1. For the requests my sample accounts were just 'email@email.com' and 'password'
   2. This should allow you to start sending the sample requests immediately
2. Install [Postman](https://www.getpostman.com/)
3. Load the postman collection from the repo into postman
4. start running and changing some of the requests
