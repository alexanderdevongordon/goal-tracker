<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/goal'], function() {
	Route::post('/read', 'GoalController@read');
	Route::post('/all', 'GoalController@all');
	Route::post('/create', 'GoalController@create');
	Route::post('/update', 'GoalController@update');
	Route::post('/delete', 'GoalController@delete');
});

Route::group(['prefix' => '/milestone'], function() {
	Route::post('/read', 'MilestoneController@read');
	Route::post('/all', 'MilestoneController@all');
	Route::post('/create', 'MilestoneController@create');
	Route::post('/update', 'MilestoneController@update');
	Route::post('/delete', 'MilestoneController@delete');
});